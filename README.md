# awesome stuff

## Talks

### Excited To Watch
[Functional Programming - Bodil Stokke (Foo Cafe 2013)](https://www.youtube.com/watch?v=DHubfS8E--o)  
[ClojureScript All the Way Down - Bodil Stokke (ClojureConf 2012)](https://www.youtube.com/watch?v=MTxNnYfWHOw)  
[Infoq Talks by Rich Hickey](http://www.infoq.com/author/Rich-Hickey)

### Glad I Watched
[What Every Hipster Should Know About Functional Programming - Bodil Stokke](https://vimeo.com/68331937)  
[Live React: Hot Reloading with Time Travel - Dan Abramov (React Europe 2015)](https://www.youtube.com/watch?v=xsSnOQynTHs)  
[Improving Your Workflow With Code Transformation - Sebastian McKenzie (React Europe 2015)](https://www.youtube.com/watch?v=OFuDvqZmUrE)
